/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package usadb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 */
 /*
 * @author explosiveamber
 */
public interface DBOps {
    
    public void con() throws SQLException;
    
    
    // INSERT
    public boolean ins(String query);
    
    // SELECT
    public ResultSet sel(String query);
    
    

}
