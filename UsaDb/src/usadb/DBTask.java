/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package usadb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;
import java.awt.Cursor;

/**
 */
 /*
 * @author explosiveamber
 */
public class DBTask extends SwingWorker<ArrayList<String>, String> implements DBOps {

    // Non c'è bisogno di fare override del metodo cancel! 
    // Si invoca e basta (vedi dispensa)
    // Ci costruiamo un istanza di questa task ogni volta che usiamo il db
    
    
    protected String sql;
    protected Connection conn;
    
    
    public DBTask(String sql){
    
        this.sql = sql;
        this.conn = null;
    }
    
    
    @Override
    protected ArrayList<String> doInBackground() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        ArrayList<String> totaleris = new ArrayList<>();
        
        int progress = 0;
        setProgress(0);
        
        try { 
            this.con();
            ResultSet ris = this.sel(sql);
            while(ris.next()){
            
               totaleris.add(ris.getString("nome"));
               
               progress += 10;
               
               Thread.sleep(1000);
               
               
            }
            
        } catch(Exception e){
        
            e.printStackTrace();
         }
        
        return totaleris;
    }
    
    @Override
    protected void done(){
    
       
    }

    @Override
    public void con() {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBTask.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(DBTask.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(DBTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost/persone?" +
                    "user=root&password=");
        } catch (SQLException ex) {
            Logger.getLogger(DBTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }

    @Override
    public boolean ins(String query) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultSet sel(String query) {
        ResultSet rs = null;
        
        try {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            PreparedStatement ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DBTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs; 
    }

}
