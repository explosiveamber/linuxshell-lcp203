/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package usadb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;
import java.awt.Cursor;
import java.awt.event.ActionEvent;

/**
 */
 /*
 * @author explosiveamber
 */
public class DBTask extends SwingWorker<ArrayList<String>, String> implements DBOps {

    // Non c'è bisogno di fare override del metodo cancel! 
    // Si invoca e basta (vedi dispensa)
    // Ci costruiamo un istanza di questa task ogni volta che usiamo il db
    
    
    protected String sql;
    protected Connection conn;
    protected ActionEvent com;
    
    
    public DBTask(String sql, ActionEvent comando){
    
        this.sql = sql;
        this.conn = null;
        this.com = comando;
    }
    
    
    @Override
    protected ArrayList<String> doInBackground() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        ArrayList<String> totaleris = new ArrayList<>();
        
        int progress = 0;
        setProgress(0);
        
        try { 
            this.con();
            ResultSet ris = null;
            
            String op = com.getActionCommand();
            
            
            switch(op){
            
                case "ins":
                    this.ins(sql);
                case "del":
                    this.del(sql);
                case "sel":
                    this.sel(sql);
                     ris = this.sel(sql);
                case "alter":
                    this.alter(sql);
                case "drop":
                    this.drop(sql);
                default:
                    
            }
            if(ris != null){
            while(ris.next()){
            
               totaleris.add(ris.getString("nome"));
               
               progress += 10;
               
               Thread.sleep(1000);
               
               
              }
            }
            
        } catch(Exception e){
        
            e.printStackTrace();
         }
        
        return totaleris;
    }
    
    @Override
    protected void done(){
    
       
    }

    @Override
    public void con() {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBTask.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(DBTask.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(DBTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost/persone2?" +
                    "user=root&password=");
        } catch (SQLException ex) {
            Logger.getLogger(DBTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }

    @Override
    public boolean ins(String query) {
        
        try {
           
            PreparedStatement ps = conn.prepareStatement(query);
            ps.executeUpdate();
            ps.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
        
    }

    @Override
    public ResultSet sel(String query) {
        ResultSet rs = null;
        
        try {
           
            PreparedStatement ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs; 
    }

    @Override
    public boolean del(String query) {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            PreparedStatement ps = conn.prepareStatement(query);
            ps.executeQuery();
            ps.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean alter(String query) {

        try {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            PreparedStatement ps = conn.prepareStatement(query);
            ps.executeUpdate();
            ps.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean drop(String query) {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            PreparedStatement ps = conn.prepareStatement(query);
            ps.executeUpdate();
            ps.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

}
